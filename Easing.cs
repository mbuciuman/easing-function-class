/*
     Original Author: Mike Buciuman-Coman
     Creation Date: 3/30/2015

     Description:
          Contains easing functions for floats, vectors and quaternions.

     Revision History:
          Mike Buciuman-Coman [3/30/2015] - Created Class.
          Mike Buciuman-Coman [12/7/2015] - Added method comments.
*/

using System.Collections;
using UnityEngine;

public enum EasingType {
     LINEAR,
     IN_QUAD,
     OUT_QUAD,
     INOUT_QUAD,
     IN_CUBIC,
     OUT_CUBIC,
     INOUT_CUBIC,
     IN_QUART,
     OUT_QUART,
     INOUT_QUART,
     IN_QUINT,
     OUT_QUINT,
     INOUT_QUINT,
     IN_SINE,
     OUT_SINE,
     INOUT_SINE,
     IN_EXPO,
     OUT_EXPO,
     INOUT_EXPO,
     IN_CIRC,
     OUT_CIRC,
     INOUT_CIRC,
     IN_ELASTIC,
     OUT_ELASTIC,
     INOUT_ELASTIC,
     IN_BACK,
     OUT_BACK,
     INOUT_BACK,
     IN_BOUNCE,
     OUT_BOUNCE,
     INOUT_BOUNCE,
     QUANT
}

[ExecuteInEditMode]
public static class Easing {
     /// <summary>
     /// Ease method for easing float values.
     /// </summary>
     /// <param name="from">Initial float value to ease from.</param>
     /// <param name="to">End float value to end easing.</param>
     /// <param name="currentTime">Initial time to start easing at.</param>
     /// <param name="totalTime">Total time to ease value.</param>
     /// <param name="easingType">How to ease the float value.</param>
     /// <param name="quantizedAmount">If the easing type is quantized, represents the amount of quantization.</param>
     /// <returns>Returns the current value eased at the given time.</returns>
     public static float Ease(float from, float to, float currentTime, float totalTime, EasingType easingType, int quantizedAmount = 0) {
          if(currentTime < 0) {
               Debug.LogError("currentTime cannot be negative! Returning initial value...");
               return from;
          }
          float returnedVal = from;
          float currentPercent;
          float percentage = 0.3f;
          float easeParameter = 1.70158f; //parameter which causes an overshoot of 10%
          if(currentTime < totalTime) {
               currentPercent = currentTime / totalTime;
               switch(easingType) {
                    case EasingType.LINEAR:
                         returnedVal = Mathf.Lerp(from, to, currentPercent);
                         break;

                    case EasingType.IN_QUAD:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 2f) + from;
                         break;

                    case EasingType.OUT_QUAD:
                         returnedVal = -(to - from) * currentPercent * (currentPercent - 2) + from;
                         break;

                    case EasingType.INOUT_QUAD:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * currentPercent * currentPercent + from;
                         else
                              returnedVal = -(to - from) / 2 * ((--currentPercent) * (currentPercent - 2) - 1) + from;
                         break;

                    case EasingType.IN_CUBIC:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 3f) + from;
                         break;

                    case EasingType.OUT_CUBIC:
                         returnedVal = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 3f)) + from;
                         break;

                    case EasingType.INOUT_CUBIC:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(currentPercent, 3f) + from;
                         else
                              returnedVal = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 2f) + 2) + from;
                         break;

                    case EasingType.IN_QUART:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 4f) + from;
                         break;

                    case EasingType.OUT_QUART:
                         returnedVal = -(to - from) * (Mathf.Pow(1 - currentPercent, 4f) - 1) + from;
                         break;

                    case EasingType.INOUT_QUART:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(currentPercent, 4f) + from;
                         else
                              returnedVal = -(to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 3f) - 2) + from;
                         break;

                    case EasingType.IN_QUINT:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 5f) + from;
                         break;

                    case EasingType.OUT_QUINT:
                         returnedVal = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 5f)) + from;
                         break;

                    case EasingType.INOUT_QUINT:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(currentPercent, 5f) + from;
                         else
                              returnedVal = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 4f) + 2) + from;
                         break;

                    case EasingType.IN_SINE:
                         returnedVal = -(to - from) * Mathf.Cos(currentPercent * Mathf.PI / 2) + to;
                         break;

                    case EasingType.OUT_SINE:
                         returnedVal = (to - from) * Mathf.Sin(currentPercent * Mathf.PI / 2) + from;
                         break;

                    case EasingType.INOUT_SINE:
                         returnedVal = -(to - from) / 2 * (Mathf.Cos(currentPercent * Mathf.PI) - 1) + from;
                         break;

                    case EasingType.IN_EXPO:
                         returnedVal = (to - from) * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         break;

                    case EasingType.OUT_EXPO:
                         returnedVal = (to - from) * (-Mathf.Pow(2, -10 * currentPercent) + 1) + from;
                         break;

                    case EasingType.INOUT_EXPO:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         else
                              returnedVal = (to - from) / 2 * (-Mathf.Pow(2, -10 * --currentPercent) + 2) + from;
                         break;

                    case EasingType.IN_CIRC:
                         returnedVal = -(to - from) * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         break;

                    case EasingType.OUT_CIRC:
                         returnedVal = (to - from) * Mathf.Sqrt(1 - Mathf.Pow(1 - currentPercent, 2)) + from;
                         break;

                    case EasingType.INOUT_CIRC:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = -(to - from) / 2 * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         else
                              returnedVal = (to - from) / 2 * (Mathf.Sqrt(1 - (currentPercent -= 2) * currentPercent) + 1) + from;
                         break;

                    case EasingType.IN_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         returnedVal = (to - from) * (-Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         break;

                    case EasingType.OUT_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         returnedVal = (to - from) * (Mathf.Pow(2, -10 * currentPercent) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage) + 1) + from;
                         break;

                    case EasingType.INOUT_ELASTIC:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              percentage = .45f;      // .3f * 1.5f
                         easeParameter = .1125f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         if(currentPercent < 1)
                              returnedVal = -(to - from) / 2 * (Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         else
                              returnedVal = (to - from) / 2 * (Mathf.Pow(2, -10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + to;
                         break;

                    case EasingType.IN_BACK:
                         returnedVal = (to - from) * (Mathf.Pow(currentPercent, 2) * ((easeParameter + 1) * currentPercent - easeParameter)) + from;
                         break;

                    case EasingType.OUT_BACK:
                         returnedVal = (to - from) * ((currentPercent -= 1) * currentPercent * ((easeParameter + 1) * currentPercent + easeParameter) + 1) + from;
                         break;

                    case EasingType.INOUT_BACK:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * (Mathf.Pow(currentPercent, 2f) * (((easeParameter * 1.525f) + 1) * currentPercent - easeParameter * 1.525f)) + from;
                         else
                              returnedVal = (to - from) / 2 * ((currentPercent -= 2) * currentPercent * (((easeParameter * 1.525f) + 1) * currentPercent + easeParameter * 1.525f)) + to;
                         break;

                    case EasingType.IN_BOUNCE:
                         currentPercent = 1 - currentPercent;
                         if(currentPercent < (1 / 2.75f)) {
                              returnedVal = (to - from) * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              returnedVal = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              returnedVal = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              returnedVal = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                         }
                         break;

                    case EasingType.OUT_BOUNCE:
                         if(currentPercent < (1 / 2.75f)) {
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + from;
                         }
                         break;

                    case EasingType.INOUT_BOUNCE:
                         if(currentPercent < .5f) {
                              currentPercent = 1 - 2 * currentPercent;
                              if(currentPercent < (1 / 2.75f)) {
                                   returnedVal = (to - from) / 2 * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   returnedVal = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   returnedVal = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   returnedVal = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                              }
                         } else {
                              currentPercent = 2 * currentPercent - 1;
                              if(currentPercent < (1 / 2.75f)) {
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2)) + (to + from) / 2;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + (to + from) / 2;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + (to + from) / 2;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + (to + from) / 2;
                              }
                         }
                         break;

                    case EasingType.QUANT:
                         if(quantizedAmount == 0 || quantizedAmount == 1)
                              returnedVal = to;
                         else
                              returnedVal = (Mathf.Floor((currentTime / totalTime) * quantizedAmount) / quantizedAmount) * (to - from) + from;
                         break;
               }
               return returnedVal;
          }
          return to;
     }

     /// <summary>
     /// Ease method for easing Vector3 values.
     /// </summary>
     /// <param name="from">Initial Vector3 value to ease from.</param>
     /// <param name="to">End Vector3 value to end easing.</param>
     /// <param name="currentTime">Initial time to start easing at.</param>
     /// <param name="totalTime">Total time to ease value.</param>
     /// <param name="easingType">How to ease the Vector3 value.</param>
     /// <returns>Returns the current value eased at the given time.</returns>
     public static Vector3 Ease(Vector3 from, Vector3 to, float currentTime, float totalTime, EasingType easingType) {
          if(currentTime < 0) {
               Debug.LogError("currentTime cannot be negative! Returning initial value...");
               return from;
          }
          Vector3 returnedVal = from;
          float currentPercent;
          float percentage = 0.3f;
          float easeParameter = 1.70158f; //parameter which causes an overshoot of 10%
          if(currentTime < totalTime) {
               currentPercent = currentTime / totalTime;
               switch(easingType) {
                    case EasingType.LINEAR:
                         returnedVal = Vector3.Lerp(from, to, currentPercent);
                         break;

                    case EasingType.IN_QUAD:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 2f) + from;
                         break;

                    case EasingType.OUT_QUAD:
                         returnedVal = -(to - from) * currentPercent * (currentPercent - 2) + from;
                         break;

                    case EasingType.INOUT_QUAD:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * currentPercent * currentPercent + from;
                         else
                              returnedVal = -(to - from) / 2 * ((--currentPercent) * (currentPercent - 2) - 1) + from;
                         break;

                    case EasingType.IN_CUBIC:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 3f) + from;
                         break;

                    case EasingType.OUT_CUBIC:
                         returnedVal = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 3f)) + from;
                         break;

                    case EasingType.INOUT_CUBIC:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(currentPercent, 3f) + from;
                         else
                              returnedVal = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 2f) + 2) + from;
                         break;

                    case EasingType.IN_QUART:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 4f) + from;
                         break;

                    case EasingType.OUT_QUART:
                         returnedVal = -(to - from) * (Mathf.Pow(1 - currentPercent, 4f) - 1) + from;
                         break;

                    case EasingType.INOUT_QUART:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(currentPercent, 4f) + from;
                         else
                              returnedVal = -(to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 3f) - 2) + from;
                         break;

                    case EasingType.IN_QUINT:
                         returnedVal = (to - from) * Mathf.Pow(currentPercent, 5f) + from;
                         break;

                    case EasingType.OUT_QUINT:
                         returnedVal = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 5f)) + from;
                         break;

                    case EasingType.INOUT_QUINT:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(currentPercent, 5f) + from;
                         else
                              returnedVal = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 4f) + 2) + from;
                         break;

                    case EasingType.IN_SINE:
                         returnedVal = -(to - from) * Mathf.Cos(currentPercent * Mathf.PI / 2) + to;
                         break;

                    case EasingType.OUT_SINE:
                         returnedVal = (to - from) * Mathf.Sin(currentPercent * Mathf.PI / 2) + from;
                         break;

                    case EasingType.INOUT_SINE:
                         returnedVal = -(to - from) / 2 * (Mathf.Cos(currentPercent * Mathf.PI) - 1) + from;
                         break;

                    case EasingType.IN_EXPO:
                         returnedVal = (to - from) * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         break;

                    case EasingType.OUT_EXPO:
                         returnedVal = (to - from) * (-Mathf.Pow(2, -10 * currentPercent) + 1) + from;
                         break;

                    case EasingType.INOUT_EXPO:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         else
                              returnedVal = (to - from) / 2 * (-Mathf.Pow(2, -10 * --currentPercent) + 2) + from;
                         break;

                    case EasingType.IN_CIRC:
                         returnedVal = -(to - from) * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         break;

                    case EasingType.OUT_CIRC:
                         returnedVal = (to - from) * Mathf.Sqrt(1 - Mathf.Pow(1 - currentPercent, 2)) + from;
                         break;

                    case EasingType.INOUT_CIRC:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = -(to - from) / 2 * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         else
                              returnedVal = (to - from) / 2 * (Mathf.Sqrt(1 - (currentPercent -= 2) * currentPercent) + 1) + from;
                         break;

                    case EasingType.IN_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         returnedVal = (to - from) * (-Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         break;

                    case EasingType.OUT_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         returnedVal = (to - from) * (Mathf.Pow(2, -10 * currentPercent) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage) + 1) + from;
                         break;

                    case EasingType.INOUT_ELASTIC:
                         currentPercent = currentTime / (totalTime / 2);
                         percentage = .45f;      // .3f * 1.5f
                         easeParameter = .1125f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         if(currentPercent < 1)
                              returnedVal = -(to - from) / 2 * (Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         else
                              returnedVal = (to - from) / 2 * (Mathf.Pow(2, -10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + to;
                         break;

                    case EasingType.IN_BACK:
                         returnedVal = (to - from) * (Mathf.Pow(currentPercent, 2) * ((easeParameter + 1) * currentPercent - easeParameter)) + from;
                         break;

                    case EasingType.OUT_BACK:
                         returnedVal = (to - from) * ((currentPercent -= 1) * currentPercent * ((easeParameter + 1) * currentPercent + easeParameter) + 1) + from;
                         break;

                    case EasingType.INOUT_BACK:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              returnedVal = (to - from) / 2 * (Mathf.Pow(currentPercent, 2f) * (((easeParameter * 1.525f) + 1) * currentPercent - easeParameter * 1.525f)) + from;
                         else
                              returnedVal = (to - from) / 2 * ((currentPercent -= 2) * currentPercent * (((easeParameter * 1.525f) + 1) * currentPercent + easeParameter * 1.525f)) + to;
                         break;

                    case EasingType.IN_BOUNCE:
                         currentPercent = 1 - currentPercent;
                         if(currentPercent < (1 / 2.75f)) {
                              returnedVal = (to - from) * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              returnedVal = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              returnedVal = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              returnedVal = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                         }
                         break;

                    case EasingType.OUT_BOUNCE:
                         if(currentPercent < (1 / 2.75f)) {
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              returnedVal = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + from;
                         }
                         break;

                    case EasingType.INOUT_BOUNCE:
                         if(currentPercent < .5f) {
                              currentPercent = 1 - 2 * currentPercent;
                              if(currentPercent < (1 / 2.75f)) {
                                   returnedVal = (to - from) / 2 * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   returnedVal = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   returnedVal = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   returnedVal = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                              }
                         } else {
                              currentPercent = 2 * currentPercent - 1;
                              if(currentPercent < (1 / 2.75f)) {
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2)) + (to + from) / 2;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + (to + from) / 2;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + (to + from) / 2;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   returnedVal = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + (to + from) / 2;
                              }
                         }
                         break;
               }
               return returnedVal;
          }
          return to;
     }

     /// <summary>
     /// Ease method for easing Quaternion values.
     /// </summary>
     /// <param name="from">Initial Vector3 value to ease from.</param>
     /// <param name="to">End Vector3 value to end easing.</param>
     /// <param name="currentTime">Initial time to start easing at.</param>
     /// <param name="totalTime">Total time to ease value.</param>
     /// <param name="easingType">How to ease the Vector3 value.</param>
     /// <returns>Returns the current value eased at the given time.</returns>
     public static Quaternion Ease(Quaternion from, Quaternion to, float currentTime, float totalTime, EasingType easingType) {
          if(currentTime < 0) {
               Debug.LogError("currentTime cannot be negative! Returning initial value...");
               return from;
          }
          float currentMovementPercent = 0f;
          Quaternion returnedVal;
          float currentPercent;
          float percentage = 0.3f;
          float easeParameter = 1.70158f; //parameter which causes an overshoot of 10%
          if(currentTime < totalTime) {
               currentPercent = currentTime / totalTime;
               switch(easingType) {
                    case EasingType.LINEAR:
                         currentMovementPercent = Mathf.Lerp(0f, 1, currentPercent);
                         break;

                    case EasingType.IN_QUAD:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 2f) + 0f;
                         break;

                    case EasingType.OUT_QUAD:
                         currentMovementPercent = -(1f - 0f) * currentPercent * (currentPercent - 2) + 0f;
                         break;

                    case EasingType.INOUT_QUAD:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * currentPercent * currentPercent + 0f;
                         else
                              currentMovementPercent = -(1f - 0f) / 2 * ((--currentPercent) * (currentPercent - 2) - 1) + 0f;
                         break;

                    case EasingType.IN_CUBIC:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 3f) + 0f;
                         break;

                    case EasingType.OUT_CUBIC:
                         currentMovementPercent = (1f - 0f) * (1f - Mathf.Pow(1f - currentPercent, 3f)) + 0f;
                         break;

                    case EasingType.INOUT_CUBIC:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(currentPercent, 3f) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 2f) + 2) + 0f;
                         break;

                    case EasingType.IN_QUART:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 4f) + 0f;
                         break;

                    case EasingType.OUT_QUART:
                         currentMovementPercent = -(1f - 0f) * (Mathf.Pow(1f - currentPercent, 4f) - 1) + 0f;
                         break;

                    case EasingType.INOUT_QUART:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(currentPercent, 4f) + 0f;
                         else
                              currentMovementPercent = -(1f - 0f) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 3f) - 2) + 0f;
                         break;

                    case EasingType.IN_QUINT:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 5f) + 0f;
                         break;

                    case EasingType.OUT_QUINT:
                         currentMovementPercent = (1f - 0f) * (1f - Mathf.Pow(1f - currentPercent, 5f)) + 0f;
                         break;

                    case EasingType.INOUT_QUINT:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(currentPercent, 5f) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 4f) + 2) + 0f;
                         break;

                    case EasingType.IN_SINE:
                         currentMovementPercent = -(1f - 0f) * Mathf.Cos(currentPercent * Mathf.PI / 2) + 1;
                         break;

                    case EasingType.OUT_SINE:
                         currentMovementPercent = (1f - 0f) * Mathf.Sin(currentPercent * Mathf.PI / 2) + 0f;
                         break;

                    case EasingType.INOUT_SINE:
                         currentMovementPercent = -(1f - 0f) / 2 * (Mathf.Cos(currentPercent * Mathf.PI) - 1) + 0f;
                         break;

                    case EasingType.IN_EXPO:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(2, 10f * (currentPercent - 1)) + 0f;
                         break;

                    case EasingType.OUT_EXPO:
                         currentMovementPercent = (1f - 0f) * (-Mathf.Pow(2, -10f * currentPercent) + 1) + 0f;
                         break;

                    case EasingType.INOUT_EXPO:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(2, 10f * (currentPercent - 1)) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * (-Mathf.Pow(2, -10f * --currentPercent) + 2) + 0f;
                         break;

                    case EasingType.IN_CIRC:
                         currentMovementPercent = -(1f - 0f) * (Mathf.Sqrt(1f - Mathf.Pow(currentPercent, 2)) - 1) + 0f;
                         break;

                    case EasingType.OUT_CIRC:
                         currentMovementPercent = (1f - 0f) * Mathf.Sqrt(1f - Mathf.Pow(1f - currentPercent, 2)) + 0f;
                         break;

                    case EasingType.INOUT_CIRC:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = -(1f - 0f) / 2 * (Mathf.Sqrt(1f - Mathf.Pow(currentPercent, 2)) - 1) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * (Mathf.Sqrt(1f - (currentPercent -= 2) * currentPercent) + 1) + 0f;
                         break;

                    case EasingType.IN_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         currentMovementPercent = (1f - 0f) * (-Mathf.Pow(2, 10f * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + 0f;
                         break;

                    case EasingType.OUT_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         currentMovementPercent = (1f - 0f) * (Mathf.Pow(2, -10f * currentPercent) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage) + 1) + 0f;
                         break;

                    case EasingType.INOUT_ELASTIC:
                         currentPercent = currentTime / (totalTime / 2);
                         percentage = .45f;      // .3f * 1.5f
                         easeParameter = .1125f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         if(currentPercent < 1)
                              currentMovementPercent = -(1f - 0f) / 2 * (Mathf.Pow(2, 10f * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * (Mathf.Pow(2, -10f * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + 1;
                         break;

                    case EasingType.IN_BACK:
                         currentMovementPercent = (1f - 0f) * (Mathf.Pow(currentPercent, 2) * ((easeParameter + 1) * currentPercent - easeParameter)) + 0f;
                         break;

                    case EasingType.OUT_BACK:
                         currentMovementPercent = (1f - 0f) * ((currentPercent -= 1) * currentPercent * ((easeParameter + 1) * currentPercent + easeParameter) + 1) + 0f;
                         break;

                    case EasingType.INOUT_BACK:
                         currentPercent = currentTime / (totalTime / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * (Mathf.Pow(currentPercent, 2f) * (((easeParameter * 1.525f) + 1) * currentPercent - easeParameter * 1.525f)) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * ((currentPercent -= 2) * currentPercent * (((easeParameter * 1.525f) + 1) * currentPercent + easeParameter * 1.525f)) + 1;
                         break;

                    case EasingType.IN_BOUNCE:
                         currentPercent = 1f - currentPercent;
                         if(currentPercent < (1f / 2.75f)) {
                              currentMovementPercent = (1f - 0f) * (1f - 7.5625f * Mathf.Pow(currentPercent, 2)) + 0f;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + 0f;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + 0f;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + 0f;
                         }
                         break;

                    case EasingType.OUT_BOUNCE:
                         if(currentPercent < (1f / 2.75f)) {
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2)) + 0f;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + 0f;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + 0f;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + 0f;
                         }
                         break;

                    case EasingType.INOUT_BOUNCE:
                         if(currentPercent < .5f) {
                              currentPercent = 1f - 2 * currentPercent;
                              if(currentPercent < (1f / 2.75f)) {
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - 7.5625f * Mathf.Pow(currentPercent, 2)) + 0f;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + 0f;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + 0f;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + 0f;
                              }
                         } else {
                              currentPercent = 2 * currentPercent - 1;
                              if(currentPercent < (1f / 2.75f)) {
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2)) + (1f + 0f) / 2;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + (1f + 0f) / 2;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + (1f + 0f) / 2;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + (1f + 0f) / 2;
                              }
                         }
                         break;
               }
               if(currentMovementPercent > 1)
                    returnedVal = Quaternion.Inverse(Quaternion.Slerp(to, from, currentMovementPercent - 1));
               else if(currentMovementPercent > 0)
                    returnedVal = Quaternion.Slerp(from, to, currentMovementPercent);
               else
                    returnedVal = Quaternion.Inverse(Quaternion.Slerp(from, to, -currentMovementPercent));
               return returnedVal;
          }
          return to;
     }

     /// <summary>
     /// Coroutine that fades a color defined as a lambda expression between two colors.
     /// </summary>
     /// <param name="from">Initial color to fade from.</param>
     /// <param name="to">Color to fade to.</param>
     /// <param name="time">Total time to fade.</param>
     /// <param name="updateValue">Action which ideally defines a color to be set to the currently eased color. Ex. (x => declaredColor = x)</param>
     /// <param name="unscaledTime">Whether or not to use unscaledTime.</param>
     /// <returns></returns>
     public static IEnumerator EaseFadeColor(Color from, Color to, float time, System.Action<Color> updateValue, bool unscaledTime = true) {
          float initialTime = (unscaledTime ? Time.unscaledTime : Time.time);
          Color valLerped = from;
          float currentPercent;
          while((unscaledTime ? Time.unscaledTime : Time.time) - initialTime < time) {
               currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / time;
               valLerped = new Color(Mathf.Lerp(from.r, to.r, currentPercent), Mathf.Lerp(from.g, to.g, currentPercent), Mathf.Lerp(from.b, to.b, currentPercent), Mathf.Lerp(from.a, to.a, currentPercent));
               updateValue(valLerped);
               yield return null;
          }
          updateValue(to);
     }

     /// <summary>
     /// Coroutine that eases a float value defined as a lambda expression between two float values.
     /// </summary>
     /// <param name="from">Initial float to ease from.</param>
     /// <param name="to">Float to ease to.</param>
     /// <param name="time">Total time to ease.</param>
     /// <param name="updateValue">Action which ideally defines a float to be set to the currently eased float. Ex. (x => declaredFloat = x)</param>
     /// <param name="unscaledTime">Whether or not to use unscaledTime.</param>
     /// <returns></returns>
     public static IEnumerator EaseFloat(float from, float to, float time, EasingType easingType, System.Action<float> updateValue, bool unscaledTime = true, int quantizedAmount = 0) {
          float initialTime = (unscaledTime ? Time.unscaledTime : Time.time);
          float valLerped = from;
          float currentPercent;
          float percentage = 0.3f;
          float easeParameter = 1.70158f; //parameter which causes an overshoot of 10%
          while((unscaledTime ? Time.unscaledTime : Time.time) - initialTime < time) {
               currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / time;
               switch(easingType) {
                    case EasingType.LINEAR:
                         valLerped = Mathf.Lerp(from, to, currentPercent);
                         break;

                    case EasingType.IN_QUAD:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 2f) + from;
                         break;

                    case EasingType.OUT_QUAD:
                         valLerped = -(to - from) * currentPercent * (currentPercent - 2) + from;
                         break;

                    case EasingType.INOUT_QUAD:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * currentPercent * currentPercent + from;
                         else
                              valLerped = -(to - from) / 2 * ((--currentPercent) * (currentPercent - 2) - 1) + from;
                         break;

                    case EasingType.IN_CUBIC:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 3f) + from;
                         break;

                    case EasingType.OUT_CUBIC:
                         valLerped = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 3f)) + from;
                         break;

                    case EasingType.INOUT_CUBIC:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(currentPercent, 3f) + from;
                         else
                              valLerped = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 2f) + 2) + from;
                         break;

                    case EasingType.IN_QUART:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 4f) + from;
                         break;

                    case EasingType.OUT_QUART:
                         valLerped = -(to - from) * (Mathf.Pow(1 - currentPercent, 4f) - 1) + from;
                         break;

                    case EasingType.INOUT_QUART:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(currentPercent, 4f) + from;
                         else
                              valLerped = -(to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 3f) - 2) + from;
                         break;

                    case EasingType.IN_QUINT:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 5f) + from;
                         break;

                    case EasingType.OUT_QUINT:
                         valLerped = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 5f)) + from;
                         break;

                    case EasingType.INOUT_QUINT:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(currentPercent, 5f) + from;
                         else
                              valLerped = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 4f) + 2) + from;
                         break;

                    case EasingType.IN_SINE:
                         valLerped = -(to - from) * Mathf.Cos(currentPercent * Mathf.PI / 2) + to;
                         break;

                    case EasingType.OUT_SINE:
                         valLerped = (to - from) * Mathf.Sin(currentPercent * Mathf.PI / 2) + from;
                         break;

                    case EasingType.INOUT_SINE:
                         valLerped = -(to - from) / 2 * (Mathf.Cos(currentPercent * Mathf.PI) - 1) + from;
                         break;

                    case EasingType.IN_EXPO:
                         valLerped = (to - from) * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         break;

                    case EasingType.OUT_EXPO:
                         valLerped = (to - from) * (-Mathf.Pow(2, -10 * currentPercent) + 1) + from;
                         break;

                    case EasingType.INOUT_EXPO:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         else
                              valLerped = (to - from) / 2 * (-Mathf.Pow(2, -10 * --currentPercent) + 2) + from;
                         break;

                    case EasingType.IN_CIRC:
                         valLerped = -(to - from) * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         break;

                    case EasingType.OUT_CIRC:
                         valLerped = (to - from) * Mathf.Sqrt(1 - Mathf.Pow(1 - currentPercent, 2)) + from;
                         break;

                    case EasingType.INOUT_CIRC:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              valLerped = -(to - from) / 2 * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         else
                              valLerped = (to - from) / 2 * (Mathf.Sqrt(1 - (currentPercent -= 2) * currentPercent) + 1) + from;
                         break;

                    case EasingType.IN_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         valLerped = (to - from) * (-Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         break;

                    case EasingType.OUT_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         valLerped = (to - from) * (Mathf.Pow(2, -10 * currentPercent) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage) + 1) + from;
                         break;

                    case EasingType.INOUT_ELASTIC:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         percentage = .45f;      // .3f * 1.5f
                         easeParameter = .1125f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         if(currentPercent < 1)
                              valLerped = -(to - from) / 2 * (Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         else
                              valLerped = (to - from) / 2 * (Mathf.Pow(2, -10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + to;
                         break;

                    case EasingType.IN_BACK:
                         valLerped = (to - from) * (Mathf.Pow(currentPercent, 2) * ((easeParameter + 1) * currentPercent - easeParameter)) + from;
                         break;

                    case EasingType.OUT_BACK:
                         valLerped = (to - from) * ((currentPercent -= 1) * currentPercent * ((easeParameter + 1) * currentPercent + easeParameter) + 1) + from;
                         break;

                    case EasingType.INOUT_BACK:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * (Mathf.Pow(currentPercent, 2f) * (((easeParameter * 1.525f) + 1) * currentPercent - easeParameter * 1.525f)) + from;
                         else
                              valLerped = (to - from) / 2 * ((currentPercent -= 2) * currentPercent * (((easeParameter * 1.525f) + 1) * currentPercent + easeParameter * 1.525f)) + to;
                         break;

                    case EasingType.IN_BOUNCE:
                         currentPercent = 1 - currentPercent;
                         if(currentPercent < (1 / 2.75f)) {
                              valLerped = (to - from) * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              valLerped = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              valLerped = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              valLerped = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                         }
                         break;

                    case EasingType.OUT_BOUNCE:
                         if(currentPercent < (1 / 2.75f)) {
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + from;
                         }
                         break;

                    case EasingType.INOUT_BOUNCE:
                         if(currentPercent < .5f) {
                              currentPercent = 1 - 2 * currentPercent;
                              if(currentPercent < (1 / 2.75f)) {
                                   valLerped = (to - from) / 2 * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   valLerped = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   valLerped = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   valLerped = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                              }
                         } else {
                              currentPercent = 2 * currentPercent - 1;
                              if(currentPercent < (1 / 2.75f)) {
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2)) + (to + from) / 2;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + (to + from) / 2;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + (to + from) / 2;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + (to + from) / 2;
                              }
                         }
                         break;

                    case EasingType.QUANT:

                         yield return new WaitForSeconds(time / quantizedAmount);
                         break;
               }
               updateValue(valLerped);
               yield return null;
          }
          updateValue(to);
     }

     /// <summary>
     /// Coroutine that eases a Vector3 value defined as a lambda expression between two Vector3 values.
     /// </summary>
     /// <param name="from">Initial Vector3 to ease from.</param>
     /// <param name="to">Vector3 to ease to.</param>
     /// <param name="time">Total time to ease.</param>
     /// <param name="updateValue">Action which ideally defines a Vector3 to be set to the currently eased Vector3. Ex. (x => declaredVector = x)</param>
     /// <param name="unscaledTime">Whether or not to use unscaledTime.</param>
     /// <returns></returns>
     public static IEnumerator EaseVector(Vector3 from, Vector3 to, float time, EasingType easingType, System.Action<Vector3> updateValue, bool unscaledTime = true) {
          float initialTime = (unscaledTime ? Time.unscaledTime : Time.time);
          Vector3 valLerped = from;
          float currentPercent;
          float percentage = 0.3f;
          float easeParameter = 1.70158f; //parameter which causes an overshoot of 10%
          while((unscaledTime ? Time.unscaledTime : Time.time) - initialTime < time) {
               currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / time;
               switch(easingType) {
                    case EasingType.LINEAR:
                         valLerped = Vector3.Lerp(from, to, currentPercent);
                         break;

                    case EasingType.IN_QUAD:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 2f) + from;
                         break;

                    case EasingType.OUT_QUAD:
                         valLerped = -(to - from) * currentPercent * (currentPercent - 2) + from;
                         break;

                    case EasingType.INOUT_QUAD:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * currentPercent * currentPercent + from;
                         else
                              valLerped = -(to - from) / 2 * ((--currentPercent) * (currentPercent - 2) - 1) + from;
                         break;

                    case EasingType.IN_CUBIC:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 3f) + from;
                         break;

                    case EasingType.OUT_CUBIC:
                         valLerped = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 3f)) + from;
                         break;

                    case EasingType.INOUT_CUBIC:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(currentPercent, 3f) + from;
                         else
                              valLerped = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 2f) + 2) + from;
                         break;

                    case EasingType.IN_QUART:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 4f) + from;
                         break;

                    case EasingType.OUT_QUART:
                         valLerped = -(to - from) * (Mathf.Pow(1 - currentPercent, 4f) - 1) + from;
                         break;

                    case EasingType.INOUT_QUART:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(currentPercent, 4f) + from;
                         else
                              valLerped = -(to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 3f) - 2) + from;
                         break;

                    case EasingType.IN_QUINT:
                         valLerped = (to - from) * Mathf.Pow(currentPercent, 5f) + from;
                         break;

                    case EasingType.OUT_QUINT:
                         valLerped = (to - from) * (1 - Mathf.Pow(1 - currentPercent, 5f)) + from;
                         break;

                    case EasingType.INOUT_QUINT:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(currentPercent, 5f) + from;
                         else
                              valLerped = (to - from) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 4f) + 2) + from;
                         break;

                    case EasingType.IN_SINE:
                         valLerped = -(to - from) * Mathf.Cos(currentPercent * Mathf.PI / 2) + to;
                         break;

                    case EasingType.OUT_SINE:
                         valLerped = (to - from) * Mathf.Sin(currentPercent * Mathf.PI / 2) + from;
                         break;

                    case EasingType.INOUT_SINE:
                         valLerped = -(to - from) / 2 * (Mathf.Cos(currentPercent * Mathf.PI) - 1) + from;
                         break;

                    case EasingType.IN_EXPO:
                         valLerped = (to - from) * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         break;

                    case EasingType.OUT_EXPO:
                         valLerped = (to - from) * (-Mathf.Pow(2, -10 * currentPercent) + 1) + from;
                         break;

                    case EasingType.INOUT_EXPO:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * Mathf.Pow(2, 10 * (currentPercent - 1)) + from;
                         else
                              valLerped = (to - from) / 2 * (-Mathf.Pow(2, -10 * --currentPercent) + 2) + from;
                         break;

                    case EasingType.IN_CIRC:
                         valLerped = -(to - from) * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         break;

                    case EasingType.OUT_CIRC:
                         valLerped = (to - from) * Mathf.Sqrt(1 - Mathf.Pow(1 - currentPercent, 2)) + from;
                         break;

                    case EasingType.INOUT_CIRC:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              valLerped = -(to - from) / 2 * (Mathf.Sqrt(1 - Mathf.Pow(currentPercent, 2)) - 1) + from;
                         else
                              valLerped = (to - from) / 2 * (Mathf.Sqrt(1 - (currentPercent -= 2) * currentPercent) + 1) + from;
                         break;

                    case EasingType.IN_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         valLerped = (to - from) * (-Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         break;

                    case EasingType.OUT_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         valLerped = (to - from) * (Mathf.Pow(2, -10 * currentPercent) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage) + 1) + from;
                         break;

                    case EasingType.INOUT_ELASTIC:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         percentage = .45f;      // .3f * 1.5f
                         easeParameter = .1125f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         if(currentPercent < 1)
                              valLerped = -(to - from) / 2 * (Mathf.Pow(2, 10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + from;
                         else
                              valLerped = (to - from) / 2 * (Mathf.Pow(2, -10 * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + to;
                         break;

                    case EasingType.IN_BACK:
                         valLerped = (to - from) * (Mathf.Pow(currentPercent, 2) * ((easeParameter + 1) * currentPercent - easeParameter)) + from;
                         break;

                    case EasingType.OUT_BACK:
                         valLerped = (to - from) * ((currentPercent -= 1) * currentPercent * ((easeParameter + 1) * currentPercent + easeParameter) + 1) + from;
                         break;

                    case EasingType.INOUT_BACK:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              valLerped = (to - from) / 2 * (Mathf.Pow(currentPercent, 2f) * (((easeParameter * 1.525f) + 1) * currentPercent - easeParameter * 1.525f)) + from;
                         else
                              valLerped = (to - from) / 2 * ((currentPercent -= 2) * currentPercent * (((easeParameter * 1.525f) + 1) * currentPercent + easeParameter * 1.525f)) + to;
                         break;

                    case EasingType.IN_BOUNCE:
                         currentPercent = 1 - currentPercent;
                         if(currentPercent < (1 / 2.75f)) {
                              valLerped = (to - from) * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              valLerped = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              valLerped = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              valLerped = (to - from) * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                         }
                         break;

                    case EasingType.OUT_BOUNCE:
                         if(currentPercent < (1 / 2.75f)) {
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + from;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + from;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              valLerped = (to - from) * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + from;
                         }
                         break;

                    case EasingType.INOUT_BOUNCE:
                         if(currentPercent < .5f) {
                              currentPercent = 1 - 2 * currentPercent;
                              if(currentPercent < (1 / 2.75f)) {
                                   valLerped = (to - from) / 2 * (1 - 7.5625f * Mathf.Pow(currentPercent, 2)) + from;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   valLerped = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + from;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   valLerped = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + from;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   valLerped = (to - from) / 2 * (1 - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + from;
                              }
                         } else {
                              currentPercent = 2 * currentPercent - 1;
                              if(currentPercent < (1 / 2.75f)) {
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2)) + (to + from) / 2;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + (to + from) / 2;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + (to + from) / 2;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   valLerped = (to - from) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + (to + from) / 2;
                              }
                         }
                         break;
               }
               updateValue(valLerped);
               yield return null;
          }
          updateValue(to);
     }

     /// <summary>
     /// Coroutine that eases a Quaternion value defined as a lambda expression between two Quaternion values.
     /// </summary>
     /// <param name="from">Initial Quaternion to ease from.</param>
     /// <param name="to">Quaternion to ease to.</param>
     /// <param name="time">Total time to ease.</param>
     /// <param name="updateValue">Action which ideally defines a Quaternion to be set to the currently eased Quaternion. Ex. (x => declaredQuaternion = x)</param>
     /// <param name="unscaledTime">Whether or not to use unscaledTime.</param>
     /// <returns></returns>
     public static IEnumerator EaseQuaternion(Quaternion from, Quaternion to, float time, EasingType easingType, System.Action<Quaternion> updateValue, bool unscaledTime = true) {
          float initialTime = (unscaledTime ? Time.unscaledTime : Time.time);
          float currentMovementPercent = 0f;
          Quaternion valLerped;
          float currentPercent;
          float percentage = 0.3f;
          float easeParameter = 1.70158f; //parameter which causes an overshoot of 10%
          while((unscaledTime ? Time.unscaledTime : Time.time) - initialTime < time) {
               currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / time;
               switch(easingType) {
                    case EasingType.LINEAR:
                         currentMovementPercent = Mathf.Lerp(0f, 1, currentPercent);
                         break;

                    case EasingType.IN_QUAD:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 2f) + 0f;
                         break;

                    case EasingType.OUT_QUAD:
                         currentMovementPercent = -(1f - 0f) * currentPercent * (currentPercent - 2) + 0f;
                         break;

                    case EasingType.INOUT_QUAD:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * currentPercent * currentPercent + 0f;
                         else
                              currentMovementPercent = -(1f - 0f) / 2 * ((--currentPercent) * (currentPercent - 2) - 1) + 0f;
                         break;

                    case EasingType.IN_CUBIC:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 3f) + 0f;
                         break;

                    case EasingType.OUT_CUBIC:
                         currentMovementPercent = (1f - 0f) * (1f - Mathf.Pow(1f - currentPercent, 3f)) + 0f;
                         break;

                    case EasingType.INOUT_CUBIC:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(currentPercent, 3f) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 2f) + 2) + 0f;
                         break;

                    case EasingType.IN_QUART:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 4f) + 0f;
                         break;

                    case EasingType.OUT_QUART:
                         currentMovementPercent = -(1f - 0f) * (Mathf.Pow(1f - currentPercent, 4f) - 1) + 0f;
                         break;

                    case EasingType.INOUT_QUART:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(currentPercent, 4f) + 0f;
                         else
                              currentMovementPercent = -(1f - 0f) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 3f) - 2) + 0f;
                         break;

                    case EasingType.IN_QUINT:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(currentPercent, 5f) + 0f;
                         break;

                    case EasingType.OUT_QUINT:
                         currentMovementPercent = (1f - 0f) * (1f - Mathf.Pow(1f - currentPercent, 5f)) + 0f;
                         break;

                    case EasingType.INOUT_QUINT:
                         currentPercent = (((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2));
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(currentPercent, 5f) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * ((currentPercent -= 2) * Mathf.Pow(currentPercent, 4f) + 2) + 0f;
                         break;

                    case EasingType.IN_SINE:
                         currentMovementPercent = -(1f - 0f) * Mathf.Cos(currentPercent * Mathf.PI / 2) + 1;
                         break;

                    case EasingType.OUT_SINE:
                         currentMovementPercent = (1f - 0f) * Mathf.Sin(currentPercent * Mathf.PI / 2) + 0f;
                         break;

                    case EasingType.INOUT_SINE:
                         currentMovementPercent = -(1f - 0f) / 2 * (Mathf.Cos(currentPercent * Mathf.PI) - 1) + 0f;
                         break;

                    case EasingType.IN_EXPO:
                         currentMovementPercent = (1f - 0f) * Mathf.Pow(2, 10f * (currentPercent - 1)) + 0f;
                         break;

                    case EasingType.OUT_EXPO:
                         currentMovementPercent = (1f - 0f) * (-Mathf.Pow(2, -10f * currentPercent) + 1) + 0f;
                         break;

                    case EasingType.INOUT_EXPO:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * Mathf.Pow(2, 10f * (currentPercent - 1)) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * (-Mathf.Pow(2, -10f * --currentPercent) + 2) + 0f;
                         break;

                    case EasingType.IN_CIRC:
                         currentMovementPercent = -(1f - 0f) * (Mathf.Sqrt(1f - Mathf.Pow(currentPercent, 2)) - 1) + 0f;
                         break;

                    case EasingType.OUT_CIRC:
                         currentMovementPercent = (1f - 0f) * Mathf.Sqrt(1f - Mathf.Pow(1f - currentPercent, 2)) + 0f;
                         break;

                    case EasingType.INOUT_CIRC:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = -(1f - 0f) / 2 * (Mathf.Sqrt(1f - Mathf.Pow(currentPercent, 2)) - 1) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * (Mathf.Sqrt(1f - (currentPercent -= 2) * currentPercent) + 1) + 0f;
                         break;

                    case EasingType.IN_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         currentMovementPercent = (1f - 0f) * (-Mathf.Pow(2, 10f * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + 0f;
                         break;

                    case EasingType.OUT_ELASTIC:
                         easeParameter = .075f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         currentMovementPercent = (1f - 0f) * (Mathf.Pow(2, -10f * currentPercent) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage) + 1) + 0f;
                         break;

                    case EasingType.INOUT_ELASTIC:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         percentage = .45f;      // .3f * 1.5f
                         easeParameter = .1125f; // percentage / (2 * Mathf.PI) * Mathf.ASin(1)
                         if(currentPercent < 1)
                              currentMovementPercent = -(1f - 0f) / 2 * (Mathf.Pow(2, 10f * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * (Mathf.Pow(2, -10f * (currentPercent -= 1)) * Mathf.Sin((currentPercent - easeParameter) * (2 * Mathf.PI) / percentage)) + 1;
                         break;

                    case EasingType.IN_BACK:
                         currentMovementPercent = (1f - 0f) * (Mathf.Pow(currentPercent, 2) * ((easeParameter + 1) * currentPercent - easeParameter)) + 0f;
                         break;

                    case EasingType.OUT_BACK:
                         currentMovementPercent = (1f - 0f) * ((currentPercent -= 1) * currentPercent * ((easeParameter + 1) * currentPercent + easeParameter) + 1) + 0f;
                         break;

                    case EasingType.INOUT_BACK:
                         currentPercent = ((unscaledTime ? Time.unscaledTime : Time.time) - initialTime) / (time / 2);
                         if(currentPercent < 1)
                              currentMovementPercent = (1f - 0f) / 2 * (Mathf.Pow(currentPercent, 2f) * (((easeParameter * 1.525f) + 1) * currentPercent - easeParameter * 1.525f)) + 0f;
                         else
                              currentMovementPercent = (1f - 0f) / 2 * ((currentPercent -= 2) * currentPercent * (((easeParameter * 1.525f) + 1) * currentPercent + easeParameter * 1.525f)) + 1;
                         break;

                    case EasingType.IN_BOUNCE:
                         currentPercent = 1f - currentPercent;
                         if(currentPercent < (1f / 2.75f)) {
                              currentMovementPercent = (1f - 0f) * (1f - 7.5625f * Mathf.Pow(currentPercent, 2)) + 0f;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + 0f;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + 0f;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + 0f;
                         }
                         break;

                    case EasingType.OUT_BOUNCE:
                         if(currentPercent < (1f / 2.75f)) {
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2)) + 0f;
                         } else if(currentPercent < (2f / 2.75f)) {
                              currentPercent -= (1.5f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + 0f;
                         } else if(currentPercent < (2.5f / 2.75f)) {
                              currentPercent -= (2.25f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + 0f;
                         } else {
                              currentPercent -= (2.625f / 2.75f);
                              currentMovementPercent = (1f - 0f) * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + 0f;
                         }
                         break;

                    case EasingType.INOUT_BOUNCE:
                         if(currentPercent < .5f) {
                              currentPercent = 1f - 2 * currentPercent;
                              if(currentPercent < (1f / 2.75f)) {
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - 7.5625f * Mathf.Pow(currentPercent, 2)) + 0f;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .75f)) + 0f;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f)) + 0f;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (1f - (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f)) + 0f;
                              }
                         } else {
                              currentPercent = 2 * currentPercent - 1;
                              if(currentPercent < (1f / 2.75f)) {
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2)) + (1f + 0f) / 2;
                              } else if(currentPercent < (2f / 2.75f)) {
                                   currentPercent -= (1.5f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .75f) + (1f + 0f) / 2;
                              } else if(currentPercent < (2.5f / 2.75f)) {
                                   currentPercent -= (2.25f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .9375f) + (1f + 0f) / 2;
                              } else {
                                   currentPercent -= (2.625f / 2.75f);
                                   currentMovementPercent = (1f - 0f) / 2 * (7.5625f * Mathf.Pow(currentPercent, 2) + .984375f) + (1f + 0f) / 2;
                              }
                         }
                         break;
               }
               if(currentMovementPercent > 1)
                    valLerped = Quaternion.Inverse(Quaternion.Slerp(to, from, currentMovementPercent - 1));
               else if(currentMovementPercent > 0)
                    valLerped = Quaternion.Slerp(from, to, currentMovementPercent);
               else
                    valLerped = Quaternion.Inverse(Quaternion.Slerp(from, to, -currentMovementPercent));
               updateValue(valLerped);
               yield return null;
          }
          updateValue(to);
     }
}